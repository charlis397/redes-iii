import hug
import telnetlib


@hug.get(examples='host=10.0.26.2')
@hug.local()
def telnet(host: hug.types.text):
	user = "caballero"
	password = "cisco"

	#Abrimos archivo donde colocaremos la informacion
	archivo = open("registro", "w+")

	#Conexion Telnet
	tn = telnetlib.Telnet(host)

	#Conexion al router
	tn.read_until(b"Username: ")
	tn.write(user.encode('ascii') + b"\n")
	if password:
	    tn.read_until(b"Password: ")
	    tn.write(password.encode('ascii') + b"\n")

	#Colocamos los comandos
	tn.write(b"terminal length 0\n")
	tn.write(b"show version\n")
	tn.write(b"end\n")
	tn.write(b"exit\n")

	#Escribimos y cerramos el archivo
	archivo.write (tn.read_all().decode('ascii'))
	archivo.close()

	return {'message': 'Conexion al router {0} correcto!'.format(host)}